const sanityBlockContentToHTML = require("@sanity/block-content-to-html")
const { graphql } = require("gatsby")

exports.createSchemaCustomization = async ({ actions }) => {
  actions.createFieldExtension({
    name: "blocktype",
    extend(options) {
      return {
        resolve(source) {
          // capitalize
          const type = source._type
          const cap = type.charAt(0).toUpperCase() + type.slice(1)
          return cap
        },
      }
    },
  })

  actions.createFieldExtension({
    name: "sanityBlockContent",
    args: {
      fieldName: "String",
    },
    extend(options) {
      return {
        resolve(source) {
          const html = sanityBlockContentToHTML({
            blocks: source[options.fieldName],
          })
          return html
        },
      }
    },
  })

  actions.createFieldExtension({
    name: "navItemType",
    args: {
      name: {
        type: "String!",
        defaultValue: "Link",
      },
    },
    extend(options) {
      return {
        resolve() {
          switch (options.name) {
            case "Group":
              return "Group"
            default:
              return "Link"
          }
        },
      }
    },
  })

  // abstract interfaces
  actions.createTypes(/* GraphQL */ `
    interface HomepageBlock implements Node {
      id: ID!
      blocktype: String
    }

    interface HomepageLink implements Node {
      id: ID!
      href: String
      text: String
    }

    interface HeaderNavItem implements Node {
      id: ID!
      navItemType: String
    }

    interface NavItem implements Node & HeaderNavItem {
      id: ID!
      navItemType: String
      href: String
      text: String
      icon: HomepageImage
      description: String
    }

    interface NavItemGroup implements Node & HeaderNavItem {
      id: ID!
      navItemType: String
      name: String
      navItems: [NavItem]
    }

    interface HomepageImage implements Node {
      id: ID!
      alt: String
      gatsbyImageData: GatsbyImageData
      url: String
    }

    interface HomepageHero implements Node & HomepageBlock {
      id: ID!
      blocktype: String
      heading: String!
      kicker: String
      subhead: String
      image: HomepageImage
      imageAlt: String
      text: String
      links: [HomepageLink]
    }

    interface PageContentItem implements Node & HomepageBlock {
      id: ID!
      blocktype: String
      heading: String
      kicker: String
      text: String
      image: HomepageImage
      imageAlt: String
      links: [HomepageLink]
    }

    interface PageContentSection implements Node & HomepageBlock {
      id: ID!
      blocktype: String
      kicker: String
      heading: String
      text: String
      content: [PageContentItem]
    }

    interface Logo implements Node {
      id: ID!
      image: HomepageImage
      alt: String
    }

    interface HomepageLogoList implements Node & HomepageBlock {
      id: ID!
      blocktype: String
      text: String
      logos: [Logo]
    }

    interface Testimonial implements Node {
      id: ID!
      quote: String
      source: String
      avatar: HomepageImage
      imageAlt: String
    }

    interface TestimonialList implements Node & HomepageBlock {
      id: ID!
      blocktype: String
      kicker: String
      heading: String
      content: [Testimonial]
    }

    interface AboutBenefit implements Node {
      id: ID!
      heading: String
      text: String
      image: HomepageImage
      imageAlt: String
    }

    interface AboutBenefitList implements Node & HomepageBlock {
      id: ID!
      blocktype: String
      heading: String
      text: String
      content: [AboutBenefit]
    }

    interface Homepage implements Node {
      id: ID!
      title: String
      description: String
      image: HomepageImage
      content: [HomepageBlock]
    }

    interface LayoutHeader implements Node {
      id: ID!
      navItems: [HeaderNavItem]
      cta: HomepageLink
    }

    enum SocialService {
      TWITTER
      FACEBOOK
      INSTAGRAM
      YOUTUBE
      LINKEDIN
      GITHUB
      DISCORD
      TWITCH
    }

    interface SocialLink implements Node {
      id: ID!
      username: String!
      service: SocialService!
    }

    interface LayoutFooter implements Node {
      id: ID!
      links: [HomepageLink]
      meta: [HomepageLink]
      socialLinks: [SocialLink]
      copyright: String
    }

    interface Layout implements Node {
      id: ID!
      header: LayoutHeader
      footer: LayoutFooter
    }

    interface AboutPage implements Node {
      id: ID!
      title: String
      description: String
      image: HomepageImage
      content: [HomepageBlock]
    }

    interface AboutProfile implements Node {
      id: ID!
      image: HomepageImage
      imageAlt: String
      name: String
      jobTitle: String
    }

    interface AboutLeadership implements Node & HomepageBlock {
      id: ID!
      blocktype: String
      kicker: String
      heading: String
      subhead: String
      content: [AboutProfile]
    }

    interface AboutLogoList implements Node & HomepageBlock {
      id: ID!
      blocktype: String
      heading: String
      links: [HomepageLink]
      logos: [Logo]
    }

    interface Page implements Node {
      id: ID!
      slug: String!
      title: String
      description: String
      image: HomepageImage
      html: String!
      sections: [HomepageBlock]
    }

    interface PageNoHtml implements Node {
      id: ID!
      slug: String!
      title: String
      description: String
      image: HomepageImage
      imageAlt: String
      sections: [HomepageBlock]
    }

    interface Banner implements Node & HomepageBlock {
      id: ID!
      blocktype: String
      heading: String
      text: String
      image: HomepageImage
    }

    interface Quadrant implements Node & HomepageBlock {
      id: ID!
      blocktype: String
      heading: String
      kicker: String
      text: String
      image: HomepageImage
      imageAlt: String
      link: HomepageLink
    }

    interface Quadrants implements Node & HomepageBlock {
      id: ID!
      blocktype: String
      kicker: String
      heading: String
      text: String
      content: [Quadrant]
    }
  `)

  // CMS-specific types for Homepage
  actions.createTypes(/* GraphQL */ `
    type SanityHomepageLink implements Node & HomepageLink {
      id: ID!
      href: String
      text: String
    }

    type SanityImageAsset implements Node & HomepageImage {
      id: ID!
      alt: String @proxy(from: "altText")
      gatsbyImageData: GatsbyImageData
      url: String
    }

    type SanityHomepage implements Node & Homepage {
      id: ID!
      title: String
      description: String
      image: HomepageImage @link(by: "id", from: "image.asset._ref")
      content: [HomepageBlock] @link
    }

    type SanityHomepageHero implements Node & HomepageHero & HomepageBlock {
      id: ID!
      _type: String
      blocktype: String @blocktype
      heading: String!
      kicker: String
      subhead: String
      image: HomepageImage @link(by: "id", from: "image.asset._ref")
      imageAlt: String
      text: String
      links: [HomepageLink] @link
    }

    type SanityPageContentItem implements Node & PageContentItem & HomepageBlock {
      id: ID!
      blocktype: String @blocktype
      heading: String
      kicker: String
      text: String
      image: HomepageImage @link(by: "id", from: "image.asset._ref")
      imageAlt: String
      links: [HomepageLink] @link
    }

    type SanityPageContentSection implements Node & PageContentSection & HomepageBlock {
      id: ID!
      blocktype: String @blocktype
      kicker: String
      heading: String
      text: String
      content: [PageContentItem]
    }

    type SanityLogo implements Node & Logo {
      id: ID!
      image: HomepageImage @link(by: "id", from: "image.asset._ref")
      alt: String
    }

    type SanityHomepageLogoList implements Node & HomepageLogoList & HomepageBlock {
      id: ID!
      blocktype: String @blocktype
      text: String
      logos: [Logo]
    }

    type SanityTestimonial implements Node & Testimonial {
      id: ID!
      quote: String
      source: String
      avatar: HomepageImage @link(by: "id", from: "avatar.asset._ref")
      imageAlt: String
    }

    type SanityTestimonialList implements Node & TestimonialList & HomepageBlock {
      id: ID!
      blocktype: String @blocktype
      kicker: String
      heading: String
      content: [Testimonial]
    }

    type SanityAboutBenefit implements Node & AboutBenefit {
      id: ID!
      heading: String
      text: String
      image: HomepageImage @link(by: "id", from: "image.asset._ref")
      imageAlt: String
    }

    type SanityAboutBenefitList implements Node & AboutBenefitList & HomepageBlock {
      id: ID!
      blocktype: String @blocktype
      heading: String
      text: String
      content: [AboutBenefit]
    }

    type SanityNavItem implements Node & NavItem & HeaderNavItem {
      id: ID!
      navItemType: String @navItemType(name: "Link")
      href: String
      text: String
      icon: HomepageImage @link(by: "id", from: "icon.asset._ref")
      description: String
    }

    type SanityNavItemGroup implements Node & NavItemGroup & HeaderNavItem {
      id: ID!
      navItemType: String @navItemType(name: "Group")
      name: String
      navItems: [NavItem] @link
    }

    type SanityLayoutHeader implements Node & LayoutHeader {
      id: ID!
      navItems: [HeaderNavItem] @link(from: "navItems._ref")
      cta: HomepageLink @link
    }

    type SanitySocialLink implements Node & SocialLink {
      id: ID!
      username: String!
      service: SocialService!
    }

    type SanityLayoutFooter implements Node & LayoutFooter {
      id: ID!
      links: [HomepageLink] @link
      meta: [HomepageLink] @link
      socialLinks: [SocialLink] @link
      copyright: String
    }

    type SanityLayout implements Node & Layout {
      id: ID!
      header: LayoutHeader
      footer: LayoutFooter
    }

    type SanityAboutPage implements Node & AboutPage {
      id: ID!
      title: String
      description: String
      image: HomepageImage @link(by: "id", from: "image.asset._ref")
      content: [HomepageBlock]
    }

    type SanityAboutProfile implements Node & AboutProfile {
      id: ID!
      image: HomepageImage @link(by: "id", from: "image.asset._ref")
      imageAlt: String
      name: String
      jobTitle: String
    }

    type SanityAboutLeadership implements Node & AboutLeadership & HomepageBlock {
      id: ID!
      blocktype: String @blocktype
      kicker: String
      heading: String
      subhead: String
      content: [AboutProfile]
    }

    type SanityAboutLogoList implements Node & AboutLogoList & HomepageBlock {
      id: ID!
      blocktype: String @blocktype
      heading: String
      links: [HomepageLink]
      logos: [Logo]
    }

    type SanityPage implements Node & Page {
      id: ID!
      slug: String! @proxy(from: "slug.current")
      title: String
      description: String
      image: HomepageImage @link(by: "id", from: "image.asset._ref")
      html: String! @sanityBlockContent(fieldName: "content")
      sections: [HomepageBlock] @link
    }

    type SanityPageNoHtml implements Node & PageNoHtml {
      id: ID!
      slug: String! @proxy(from: "slug.current")
      title: String
      description: String
      image: HomepageImage @link(by: "id", from: "image.asset._ref")
      imageAlt: String
      sections: [HomepageBlock] @link
    }

    type SanityBanner implements Node & Banner & HomepageBlock @dontInfer {
      id: ID!
      blocktype: String @blocktype
      heading: String
      text: String
      image: HomepageImage
    }

    type SanityQuadrant implements Node & Quadrant & HomepageBlock {
      id: ID!
      blocktype: String @blocktype
      heading: String
      kicker: String
      text: String
      image: HomepageImage @link(by: "id", from: "image.asset._ref")
      imageAlt: String
      link: HomepageLink @link
    }

    type SanityQuadrants implements Node & Quadrants & HomepageBlock {
      id: ID!
      blocktype: String @blocktype
      kicker: String
      heading: String
      text: String
      content: [Quadrant]
    }
  `)
}

exports.createPages = async ({ graphql, actions }) => {
  const { createSlice } = actions

  createSlice({
    id: "header",
    component: require.resolve("./src/components/header.js"),
  })
  createSlice({
    id: "footer",
    component: require.resolve("./src/components/footer.js"),
  })

}

//   const { createPage } = actions

//   const result = await graphql(`
//     {
//       allSanityPage {
//         edges {
//           node {
//             id
//             title
//             description
//             slug
//             image {
//               alt
//               gatsbyImageData
//             }
//           }
//         }
//       }
//     }
//   `)

//   if (result.errors) {
//     throw result.errors
//   }

//   const pages = result.data.allSanityPage.edges || []
//   pages.forEach((edge, index) => {
//   const path = `/${edge.node.slug}`

//     createPage({
//       path,
//       component: require.resolve('./src/templates/blog-post.js'),
//       context: {slug: edge.node.slug},
//     })
//   })
// }

// exports.createPages = async ({ graphql, actions }) => {
//   const { createSlice } = actions

//   createSlice({
//     id: "header",
//     component: require.resolve("./src/components/header.js"),
//   })
//   createSlice({
//     id: "footer",
//     component: require.resolve("./src/components/footer.js"),
//   })

//   const { createPage } = actions
  
//   const resultNoHtml = await graphql(`
//     {
//       allSanityPageNoHtml {
//         edges {
//           node {
//             id
//             title
//             description
//             slug
//             image {
//               alt
//               gatsbyImageData
//             }
//             imageAlt
//           }
//         }
//       }
//     }
//   `)

//   if (resultNoHtml.errors) {
//     throw resultNoHtml.errors
//   }

//   const pagesNoHtml = resultNoHtml.data.allSanityPageNoHtml.edges || []
//   pagesNoHtml.forEach((edge, index) => {
//   const path = `/${edge.node.slug}`

//     createPage({
//       path,
//       component: require.resolve('./src/templates/blog-post.js'),
//       context: {slug: edge.node.slug},
//     })
//   })

  
// }
      