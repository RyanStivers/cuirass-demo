import { createGlobalTheme, globalFontFace } from "@vanilla-extract/css"
import { colors } from "./colors.css"

export type SpaceTokens = 0 | 1 | 2 | 3 | 4 | 5 | 6
export type Space = Record<SpaceTokens, string>

const raj = 'GlobalRaj';
const rajLight = 'GlobalRajLight';

globalFontFace(raj, {
  src: 'url(/fonts/Rajdhani-Medium.woff2) format("woff2")',
  fontDisplay: "swap",
});

globalFontFace(rajLight, {
  src: 'url(/fonts/Rajdhani-Regular.woff2) format("woff2")',
  fontDisplay: "swap",
});

const space = {
  0: "0",
  1: "4px",
  2: "8px",
  3: "16px",
  4: "32px",
  5: "64px",
  6: "128px",
}

// add negative margins
Object.assign(
  space,
  Object.entries(space).reduce(
    (a, [key, val]) => ({
      ...a,
      [-1 * Number(key)]: `-${val}`,
    }),
    {}
  )
)

const fontSizes = {
  0: "12px",
  1: "14px",
  2: "16px",
  3: "18px",
  4: "24px",
  5: "32px",
  6: "48px",
  7: "64px",
}

const fontWeights = {
  normal: "400",
  medium: "500",
  semibold: "600",
  bold: "700",
  extrabold: "800",
}

const fonts = {
  text: raj + ', sans-serif',
  heading: raj + ', sans-serif',
  mono: rajLight + ", Menlo, monospace",
}

const lineHeights = {
  text: "1.65",
  heading: "1.25",
  tight: "1.1",
  solid: "1",
}

const letterSpacings = {
  normal: "0",
  tight: "-0.02em",
  wide: "0.08em",
}

const sizes = {
  container: "1280px",
  narrow: "1024px",
  wide: "1440px",
  tight: "848px",
  avatar: "48px",
  navGroupBoxMin: "300px",
  navGroupBoxMax: "400px",
  navIcon: "32px",
  navIconSmall: "30px",
}

export type Radii = "button" | "large" | "circle"

const radii: Record<Radii, string> = {
  button: "10px",
  large: "24px",
  circle: "99999px",
}

const shadows = {
  verySmall: "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)",
  small: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)",
  medium: "0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)",
  large: "0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)",
  veryLarge: "0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)",
}

export const theme = createGlobalTheme(":root", {
  colors,
  space,
  fontSizes,
  fontWeights,
  fonts,
  lineHeights,
  letterSpacings,
  sizes,
  radii,
  shadows,
})
