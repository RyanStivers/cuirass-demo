export const colors = {
  background: "#082032",
  text: "rgba(255, 255, 2555, 0.75)",
  textSecondary: "rgba(255, 255, 2555, 0.6)",
  primary: "#000000",
  muted: "#0C1620",
  active: "#e6e6e6",
  black: "#000",
  panelDark: "#2C394B",
  panelLight: "#334756",
  tertiary:  "#428BC4",
}
