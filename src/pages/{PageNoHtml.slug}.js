import * as React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import { Container, Box, Heading } from "../components/ui"
import SEOHead from "../components/head"
import Fallback from "../components/fallback"
import * as sections from "../components/sections"
import * as styles from "../components/page.css"

export default function PageNoHtml(props) {
  const { pageNoHtml } = props.data

  return (
    <Layout>
      <Box paddingY={5}>
        <Container width="narrow">
          <Heading as="h1" className={styles.titleCenter}>{pageNoHtml.title}</Heading>
        </Container>
      </Box>
      {pageNoHtml.blocks.map((block) => {
        const { id, blocktype, ...componentProps } = block
        const Component = sections[blocktype] || Fallback
        return <Component key={id} {...componentProps} />
      })}
    </Layout>
  )
}
export const Head = (props) => {
  const { pageNoHtml } = props.data
  return <SEOHead {...pageNoHtml} />
}
export const query = graphql`
  query PageNoHtmlContent($id: String!) {
    pageNoHtml(id: { eq: $id }) {
      id
      title
      slug
      description
      image {
        id
        url
      }
      imageAlt
      blocks: sections {
        id
        blocktype
        ...QuadrantsContent
        ...QuadrantContent
        ...HomepageHeroContent
        ...PageContentSectionContent
        ...AboutLogoListContent
        ...TestimonialListContent
      }
    }
  }
`
