import * as React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import { Container, Box, Heading } from "../components/ui"
import SEOHead from "../components/head"
import Fallback from "../components/fallback"
import * as sections from "../components/sections"
import * as styles from "../components/page.css"

export default function Page(props) {
  const { page } = props.data

  return (
    <Layout>
      <Box paddingY={5}>
        <Container width="narrow">
          <Heading as="h1" className={styles.titleCenter}>{page.title}</Heading>
          <div
            dangerouslySetInnerHTML={{
              __html: page.html,
            }}
          />
        </Container>
      </Box>
      {page.blocks.map((block) => {
        const { id, blocktype, ...componentProps } = block
        const Component = sections[blocktype] || Fallback
        return <Component key={id} {...componentProps} />
      })}
    </Layout>
  )
}
export const Head = (props) => {
  const { page } = props.data
  return <SEOHead {...page} />
}
export const query = graphql`
  query PageContent($id: String!) {
    page(id: { eq: $id }) {
      id
      title
      slug
      description
      image {
        id
        url
      }
      html
      blocks: sections {
        id
        blocktype
        ...QuadrantsContent
        ...QuadrantContent
        ...HomepageHeroContent
        ...PageContentSectionContent
        ...HomepageLogoListContent
        ...TestimonialListContent
      }
    }
  }
`
