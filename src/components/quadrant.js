import * as React from "react"
import { graphql } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import {
  Section,
  Box,
  Subhead,
  Kicker,
  Text,
} from "./ui"
import * as styles from "./quadrant.css"

export default function Quadrant(props) {
  return (
    <Section className={styles.gridItemWrapper}>
      <a href={props.link.href} aria-label={"Go to page: "+props.heading}>
        <div className={styles.gridItem}>
          <Box className={styles.gridItemText}>
            <Subhead>
              {props.kicker && <Kicker>{props.kicker}</Kicker>}
              {props.heading}
            </Subhead>
            <Text variant="lead">{props.text}</Text>
          </Box>
            <Box className={styles.gridItemImage} order={props.flip ? 1 : null}>
              {props.image && (
                <GatsbyImage
                alt={props.image ? props.imageAlt : "Cuirass Quadrant Image"}
                  image={getImage(props.image.gatsbyImageData)}
                />
              )}
            </Box>
        </div>
      </a>
    </Section>
  )
}

export const query = graphql`
  fragment QuadrantContent on Quadrant {
    id
    kicker
    heading
    text
    link {
      id
      href
      text
    }
    image {
      id
      gatsbyImageData
      alt
    }
    imageAlt
  }
`
