import * as React from "react"
import { graphql } from "gatsby"
import { Container, Box, Kicker, Heading, Text } from "./ui"
import Quadrant from "./quadrant"
import * as styles from "./quadrants.css"

export default function Quadrants(props) {
  return (
    <Container width="fullbleed">
      <Box>
        <Box center paddingY={5}>
          <Heading>
            {props.kicker && <Kicker>{props.kicker}</Kicker>}
            {props.heading}
          </Heading>
          {props.text && <Text>{props.text}</Text>}
        </Box>
        <Box background="panelDark" shadow="large" radius="button">
        <div className={styles.quadrantGrid}>
            {props.content.map((quadrant, i) => (
            <Quadrant key={quadrant.id} {...quadrant} flip={Boolean(i % 2)} />
            ))}
        </div>
        </Box>
      </Box>
    </Container>
  )
}

export const query = graphql`
  fragment QuadrantsContent on Quadrants {
    id
    kicker
    heading
    text
    content {
      id
      ...QuadrantContent
    }
  }
`
