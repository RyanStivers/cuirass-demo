import { style } from "@vanilla-extract/css"

export const quadrantGrid = style({
    display: "grid",
    gridTemplateColumns: "repeat(2,1fr)",
    gridTemplateRows: "repeat(2,1fr)",
    gridColumnGap: "0px",
    gridRowGap: "0px",
    backgroundColor: "#000000",
    minHeight: "0",
    minWidth: "0",
})

