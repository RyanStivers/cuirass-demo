import * as React from "react"
import { graphql } from "gatsby"
import { Container, Box, Kicker, Heading, Text } from "./ui"
import PageContentItem from "./pageContentItem"

export default function PageContentSection(props) {
  return (
    <Container width="fullbleed">
      <Box background="panelLight" shadow="medium" radius="button">
        <Box center paddingY={5}>
          <Heading>
            {props.kicker && <Kicker>{props.kicker}</Kicker>}
            {props.heading}
          </Heading>
          {props.text && <Text>{props.text}</Text>}
        </Box>
        {props.content.map((feature, i) => (
          <PageContentItem key={feature.id} {...feature} flip={Boolean(i % 2)} />
        ))}
      </Box>
    </Container>
  )
}

export const query = graphql`
  fragment PageContentSectionContent on PageContentSection {
    id
    kicker
    heading
    text
    content {
      id
      ...PageContentItemContent
    }
  }
`
