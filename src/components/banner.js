import * as React from "react"
import { graphql } from "gatsby"
import { Section, Container, Heading, Text } from "./ui"
import * as styles from "./banner.css"
import { StaticImage } from "gatsby-plugin-image"

export default function Banner(props) {
  return (
      <Section className={styles.fullScreenBanner}>
        <video playsInline autoPlay muted loop poster="images/logo.png" id="bgvid" className={styles.fullScreenBannerVideo}>
          <source src="videos/cuirass.webm" type="video/webm"/>
          <source src="videos/cuirass.mp4" type="video/mp4"/>
        </video>
        <Container className={styles.bannerTextWrapper}>
            <StaticImage
            className={styles.bannerLogo}
            src="../../static/images/logo.png" 
            alt="Cuirass Logo" 
            placeholder="blurred"
            layout="fixed"
            width={200}
            height={200}
            />
            <Heading className={styles.bannerTitle}>
              {props.heading}
            </Heading>
            <Text className={styles.bannerSubtext}>{props.text}</Text>
        </Container>
      </Section>
  )
}

  export const query = graphql`
     fragment BannerContent on Banner {
       id
       heading
       text
     }
   `