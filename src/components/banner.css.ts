import { style, fontFace } from "@vanilla-extract/css"

const aqua = fontFace({
  src: 'url(/fonts/AquaGrotesque.woff2) format("woff2")',
  fontDisplay: "swap",
});

export const fullScreenBanner = style({
  height: "95vh",
  position: "relative",
  overflow: "hidden",
})

export const fullScreenBannerVideo = style ({
  objectFit: "cover",
  width: "100%",
  height: "100%",
  position: "absolute",
  zIndex: 0,
  WebkitMaskImage: "linear-gradient(to top, rgba(0, 0, 0, .3), rgba(0, 0, 0, .7))",
  maskImage: "linear-gradient(to top, rgba(0, 0, 0, .3), rgba(0, 0, 0, .7))",
})

export const bannerTextWrapper = style({
    color: "#fff",
    minHeight: "100%",
    paddingBottom: "10rem",
    paddingTop: "12rem",
    textAlign: "center",
    position: "inherit",
    zIndex: 1,
  })

export const bannerTitle = style({
  fontFamily: aqua + ", sans-serif",
  fontSize: "5.5em",
  fontWeight: "unset",
  '@media': {
    'screen and (max-width: 800px)': {
        fontSize: "4em"
    },
    'screen and (max-width: 512px)': {
        fontSize: "2em"
    },
    'screen and (max-width: 300px)': {
        fontSize: "1em"
    }
}
})  

export const bannerSubtext = style({
  fontSize: "1.5em",
  fontWeight: "unset",
  '@media': {
    'screen and (max-width: 800px)': {
        fontSize: "1.25em"
    },
    'screen and (max-width: 512px)': {
        fontSize: "1em"
    },
    'screen and (max-width: 300px)': {
        fontSize: ".75em"
    }
  }
})  

export const bannerLogo = style({
  display: "inline-block"
})

