import { style } from "@vanilla-extract/css"

export const gridItemWrapper = style({
    textAlign: "center",
    marginLeft: "auto",
    marginRight: "auto",
    overflow: "hidden",
    minHeight: "0",
    minWidth: "0"
})

export const gridItem = style({
    width: "100%",
    overflow: "hidden",
    position: "relative",
    float: "left",
    display: "inline-block",
    cursor: "pointer",
    minHeight: "0",
    minWidth: "0"
})

export const gridItemText = style({
    position: "absolute",
    zIndex: "2",
    color: "#ffffff",
    fontSize: "24px",
    fontWeight: "bold",
    bottom: "0",
    left: "0",
    visibility: "hidden",
    opacity: "0",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    WebkitTransition: "visibility 0s, opacity 0.5s linear;",
    MozTransition: "visibility 0s, opacity 0.5s linear;",
    OTransition: "visibility 0s, opacity 0.5s linear;",
    transition: "visibility 0s, opacity 0.5s linear;",
    padding: "5%",
    "selectors": {
        [`${gridItem}:hover &`]: {
            visibility: "visible",
            opacity: "1",
        },
    },
})

export const gridItemImage = style({
    height: "100%",
    width: "100%",
    WebkitTransition: "all 0.5s",
    MozTransition: "all 0.5s",
    OTransition: "all 0.5s",
    transition: "all 0.5s",
    overflow: "hidden",
    "selectors": {
        [`${gridItem}:hover &`]: {
            msTransform: "scale(1.2)",
            WebkitTransform: "scale(1.2)",
            OTransform: "scale(1.2)",
            transform: "scale(1.2)",
            filter: "blur(1px)"
        },
    },
})

