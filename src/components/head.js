import * as React from "react"

export default function Head({ title, description, image }) {
  console.log({image})
  return (
    <>
      <meta charSet="utf-8" />
      <meta http-equiv="content-language" content="en-us"></meta>
      <title>{title}</title>
      {description && ( <meta name="description" property="og:description" content={description} /> )}
      <meta name="image" content={image ? image.url : "https://www.cuirass.net/images/logo.png"} /> 
      <meta name="keywords" content="cuirass entertainment, cuirass, end's reach, ends reach, atari, vcs, atari vcs, indie, video game, game, 3D, game development, game dev, game studio, nova scotia, NS, halifax" />
      <meta name="viewport" content="width=device-width, initial-scale=1"/>
      <meta property="og:title" content={title} />
      {image && <meta property="og:image" content={image ? image.url : "https://www.cuirass.net/images/logo.png"} />}
      <meta property="og:image" content={image ? image.url : "https://www.cuirass.net/images/logo.png"} />
      <meta property="og:url" content="https://www.cuirass.net" />
      <meta property="og:type" content="website" />
      <meta property="og:description" content={description} />
      <meta property="og:locale" content="en-us" />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:title" content={title} />
      <meta name="twitter:url" content="https://www.cuirass.net" />
      {image && <meta name="twitter:image" content={image ? image.url : "https://www.cuirass.net/images/logo.png"} />}
      <meta name="twitter:image" content={image ? image.url : "https://www.cuirass.net/images/logo.png"} />
      <meta name="twitter:creator" content="@cuirassSocial" />
      {description && <meta name="twitter:description" content={description} />}
      <meta name="twitter:description" content={description} /> 
      <link rel="icon" href="https://www.cuirass.net/images/favicon-32x32.png" />
    </>
  )
}
