import { style } from "@vanilla-extract/css"
import { theme } from "../theme.css"

export const imageWrapper = style({
  borderRadius: theme.radii.button
})