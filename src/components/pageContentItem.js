import * as React from "react"
import { graphql } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import {
  Container,
  Section,
  Flex,
  Box,
  Subhead,
  Kicker,
  Text,
  ButtonList,
} from "./ui"
import * as styles from "./pageContentSection.css"

export default function PageContentItem(props) {
  return (
    <Section padding={4} >
      <Container>
        <Flex gap={4} variant="responsive">
          <Box width={props.image ? "half" : "none"} order={props.flip ? 1 : null}>
            {props.image && (
              <GatsbyImage
                className={styles.imageWrapper}
                alt={props.image ? props.imageAlt : "Content Image"}
                image={getImage(props.image.gatsbyImageData)}
              />
            )}
          </Box>
          <Box width={props.image ? "half" : "full"}>
            <Subhead>
              {props.kicker && <Kicker>{props.kicker}</Kicker>}
              {props.heading}
            </Subhead>
            <Text variant="lead">{props.text}</Text>
            <ButtonList links={props.links} />
          </Box>
        </Flex>
      </Container>
    </Section>
  )
}

export const query = graphql`
  fragment PageContentItemContent on PageContentItem {
    id
    kicker
    heading
    text
    links {
      id
      href
      text
    }
    image {
      id
      gatsbyImageData
      alt
    }
    imageAlt
  }
`
