export default {
    name: "quadrants",
    title: "Quadrants Group",
    type: "document",
    fields: [
        { title: "Heading", name: "heading", type: "string" },
        { title: "Kicker", name: "kicker", type: "string" },
        { title: "Text", name: "text", type: "string" },
        {
        title: "Content",
        name: "content",
        description: "Only select 4 quadrant items per quadrants group",
        type: "array",
        of: [
            {
            type: "reference",
            to: [{ type: "quadrant" }],
            },
        ],
        },
    ],
  }