export default {
  name: "aboutProfile",
  title: "About Profile",
  type: "document",
  fields: [
    { title: "Image", name: "image", type: "image" },
    { title: "Image Alt", name: "imageAlt", type: "string" },
    { title: "Name", name: "name", type: "string" },
    { title: "Job Title", name: "jobTitle", type: "string" },
  ],
}
