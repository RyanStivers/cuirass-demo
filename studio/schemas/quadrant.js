export default {
    name: "quadrant",
    title: "Quadrant Item",
    type: "document",
    fields: [
      { title: "Kicker", name: "kicker", type: "string" },
      { title: "Heading", name: "heading", type: "string" },
      { title: "Text", name: "text", type: "string" },
      { title: "Image", name: "image", type: "image" },
      { title: "Image Alt", name: "imageAlt", type: "string" },
      {
        title: "Link",
        name: "link",
        type: "reference",
        to: [{ type: "homepageLink" }],
      },
    ],
  }