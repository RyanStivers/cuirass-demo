// First, we must import the schema creator
import createSchema from "part:@sanity/base/schema-creator"

// Then import schema types from any plugins that might expose them
import schemaTypes from "all:part:@sanity/base/schema-type"

import homepage from "./homepage"
import homepageLink from "./homepageLink"
import homepageHero from "./homepageHero"
import pageContentItem from "./pageContentItem"
import pageContentSection from "./pageContentSection"
import logo from "./logo"
import homepageLogoList from "./homepageLogoList"
import testimonial from "./testimonial"
import testimonialList from "./testimonialList"
import banner from "./banner"
import quadrant from "./quadrant"
import quadrants from "./quadrants"

import navItem from "./navItem"
import navItemGroup from "./navItemGroup"
import socialLink from "./socialLink"
import layoutHeader from "./layoutHeader"
import layoutFooter from "./layoutFooter"
import layout from "./layout"

import page from "./page"
import pageNoHtml from "./pageNoHtml"

import aboutPage from "./aboutPage"
import aboutProfile from "./aboutProfile"
import aboutLeadership from "./aboutLeadership"
import aboutLogoList from "./aboutLogoList"
import aboutBenefit from "./aboutBenefit"
import aboutBenefitList from "./aboutBenefitList"

// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
  // We name our schema
  name: "default",
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
    homepage,
    homepageLink,
    homepageHero,
    pageContentItem,
    pageContentSection,
    logo,
    homepageLogoList,
    testimonial,
    testimonialList,
    banner,
    quadrant,
    quadrants,
    // layout
    navItem,
    navItemGroup,
    socialLink,
    layoutHeader,
    layoutFooter,
    layout,
    // HTML page
    page,
    // No HTML page, nice
    pageNoHtml,
    // about page
    aboutPage,
    aboutProfile,
    aboutLeadership,
    aboutLogoList,
    aboutBenefit,
    aboutBenefitList,
  ]),
})
