export default {
    name: "banner",
    title: "Banner",
    type: "document",
    fields: [
      { title: "Heading", name: "heading", type: "string" },
      { title: "Text", name: "text", type: "string" },
      { title: "Image", name: "image", type: "image"},
    ],
  }