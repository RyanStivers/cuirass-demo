export default {
  name: "aboutBenefit",
  title: "About Benefit",
  type: "document",
  fields: [
    { title: "Heading", name: "heading", type: "string" },
    { title: "Image", name: "image", type: "image" },
    { title: "Image Alt", name: "imageAlt", type: "string" },
    { title: "Text", name: "text", type: "string" },
  ],
}
