export default {
  name: "logo",
  title: "Logo",
  type: "document",
  fields: [
    { title: "Image", name: "image", type: "image" },
    { title: "Alt", name: "alt", type: "string" },
  ],
}
