export default {
    name: "pageNoHtml",
    title: "Page - No HTML",
    description: "Page without an HTML Section",
    type: "document",
    fields: [
      { title: "Title", name: "title", type: "string" },
      {
        title: "Slug",
        name: "slug",
        type: "slug",
      },
      { title: "Description", name: "description", type: "string" },
      { title: "Image", name: "image", type: "image" },
      { title: "Image Alt", name: "imageAlt", type: "string" },
      {
        title: "Sections",
        name: "sections",
        type: "array",
        of: [
          {
            type: "reference",
            to: [
              { type: "homepageHero" },
              { type: "pageContentItem" },
              { type: "pageContentSection" },
              { type: "testimonialList" },
              { type: "quadrant" },
              { type: "quadrants" },
            ],
          },
        ],
      },
    ],
  }