export default {
  name: "page",
  title: "Page",
  description: "Page with an HTML Section",
  type: "document",
  fields: [
    { title: "Title", name: "title", type: "string" },
    {
      title: "Slug",
      name: "slug",
      type: "slug",
    },
    { title: "Description", name: "description", type: "string" },
    { title: "Image", name: "image", type: "image" },
    {
      title: "Content",
      name: "content",
      type: "array",
      of: [{ type: "block" }],
    },
    {
      title: "Sections",
      name: "sections",
      type: "array",
      of: [
        {
          type: "reference",
          to: [
            { type: "homepageHero" },
            { type: "pageContentItem" },
            { type: "pageContentSection" },
            { type: "testimonialList" },
            { type: "quadrant" },
            { type: "quadrants" },
          ],
        },
      ],
    },
  ],
}
