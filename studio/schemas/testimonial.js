export default {
  name: "testimonial",
  title: "Testimonial",
  type: "document",
  fields: [
    { title: "Quote", name: "quote", type: "string" },
    { title: "Source", name: "source", type: "string" },
    { title: "Avatar", name: "avatar", type: "image" },
    { title: "Image Alt", name: "imageAlt", type: "string" },
  ],
}
