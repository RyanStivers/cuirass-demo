require("dotenv").config()
require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  siteMetadata: {
    siteUrl: `https://www.cuirass.net`,
    },
    plugins: [
      {
        resolve: "gatsby-source-sanity",
        options: {
          projectId: process.env.SANITY_PROJECT_ID,
          dataset: process.env.SANITY_PROJECT_DATASET,
          token: process.env.SANITY_READ_TOKEN,
        },
      },
      "gatsby-plugin-sharp",
      "gatsby-plugin-image",
      "gatsby-transformer-sharp",
      "gatsby-plugin-vanilla-extract",
      "gatsby-plugin-sitemap",
      {
        resolve: 'gatsby-plugin-robots-txt',
        options: {
          host: 'https://www.cuirass.net',
          sitemap: 'https://www.cuirass.net/sitemap-index.xml',
          policy: [{userAgent: '*', allow: '/'}]
        }
      },
      {
        resolve: `gatsby-plugin-canonical-urls`,
        options: {
          siteUrl: `https://www.cuirass.net`,
        },
      },
      {
        resolve: "gatsby-plugin-manifest",
        options: {
          name: "Cuirass Entertainment - Official Site",
          short_name: "Cuirass Entertainment",
          start_url: "/",
          background_color: "#082032",
          theme_color: "#428bc4",
          icon: "src/favicons/favicon-32x32.png",
          icons: [
            {
              src: `src/favicons/android-chrome-192x192.png`,
              sizes: `192x192`,
              type: `image/png`,
            },
            {
              src: `/favicons/android-chrome-512x512.png`,
              sizes: `512x512`,
              type: `image/png`,
            },
            {
              src: `src/favicons/favicon-16x16.png`,
              sizes: `16x16`,
              type: `image/png`,
            },
            {
              src: `/favicons/favicon-32x32.png`,
              sizes: `32x32`,
              type: `image/png`,
            },
          ]
        },
      },
    ],
  }
